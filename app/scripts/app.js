'use strict';

/**
 * @ngdoc overview
 * @name desafioMuchmoreApp
 * @description
 * # desafioMuchmoreApp
 *
 * Main module of the application.
 */
angular
  .module('desafioMuchmoreApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'smart-table'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.otherwise('/stores');

      var starredState = {
        name: 'starred',
        url: '/starred',
        templateUrl: 'views/starred.html',
        controller: 'StarredCtrl'
      };

      var categoriesState = {
        name: 'categories',
        url: '/categories',
        templateUrl: 'views/categories.html',
        controller: 'CategoriesCtrl'
      };

      var storesState = {
        name: 'stores',
        url: '/stores',
        templateUrl: 'views/stores.html',
        controller: 'StoresCtrl'
      };

      $stateProvider.state(storesState);
      $stateProvider.state(starredState);
      $stateProvider.state(categoriesState);
  });
