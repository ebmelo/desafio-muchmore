'use strict';

/**
 * @ngdoc function
 * @name desafioMuchmoreApp.controller:StarredCtrl
 * @description
 * # StarredCtrl
 * Controller of the desafioMuchmoreApp
 */
angular.module('desafioMuchmoreApp')
  .controller('StarredCtrl', function ($rootScope, $scope) {

    $scope.starred = [];

    /**
    * Load starred stores.
    */
    function loadStarred(){
      var stores = $rootScope.stores;

      if(!stores){
        var storesPromise = StoresService.loadStores();

        storesPromise.then(
          function(response){
            $rootScope.stores = response.data;

            $scope.starred = filterStarred($rootScope.stores);
          },
          function(error){
            alert(error);
          }
        );
      } else {
        $scope.starred = filterStarred(stores);
      }
    }

    /**
    * @param {Array} stores a collection of stores
    * @return a filtered collection of stores (based on star field)
    */
    function filterStarred(stores){
      var starredStores = [];

      for(var i = 0, n = stores.length; i < n; i++){
        if(stores[i].star === true){
          starredStores.push(stores[i]);
        }
      }

      return starredStores;
    }

    loadStarred();
  });
