'use strict';

/**
 * @ngdoc function
 * @name desafioMuchmoreApp.controller:CategoriesCtrl
 * @description
 * # CategoriesCtrl
 * Controller of the desafioMuchmoreApp
 */
angular.module('desafioMuchmoreApp')
  .controller('CategoriesCtrl', function ($rootScope, $scope, StoresService) {

    $scope.categories = [];

    /**
    * Loads all categories
    */
    function loadCategories(){
      var stores = $rootScope.stores;

      if(!stores){
        var storesPromise = StoresService.loadStores();

        storesPromise.then(
          function(response){
            $rootScope.stores = response.data;

            $scope.categories = getCategories($rootScope.stores);
          },
          function(error){
            alert(error);
          }
        );
      } else {
        $scope.categories = getCategories(stores);
      }
    }

    /**
    * @param {Array} stores a collection of stores
    * @return an array of categories
    */
    function getCategories(stores){
      var added = [];
      var categories = [];

      for(var i = 0, n = stores.length; i < n; i++){
        var store = stores[i];
        var storeCategories = store.category;

        for(var j = 0, k = storeCategories.length; j < k; j++){
          var id = storeCategories[j].id;

          if(added.indexOf(id) == -1){
            added.push(id);
            categories.push(storeCategories[j]);
          }
        }
      }

      return categories;
    }

    loadCategories();
  });
