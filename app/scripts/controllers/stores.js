'use strict';

/**
 * @ngdoc function
 * @name desafioMuchmoreApp.controller:StoresCtrl
 * @description
 * # StoresCtrl
 * Controller of the desafioMuchmoreApp
 */
angular.module('desafioMuchmoreApp')
  .controller('StoresCtrl', function ($scope, $rootScope, AppConfig, StoresService) {

    $scope.serviceUrl = AppConfig.serviceUrl;

    /**
    * Loads a collection of stores.
    */
    function loadStores(){
      var storesPromise = StoresService.loadStores();

      storesPromise.then(
        function(response){
          $rootScope.stores = response.data;
        },
        function(error){
          alert(error);
        }
      );
    }

    // Load only once
    if(!$rootScope.stores){
      loadStores();
    }
  });
