'use strict';

/**
 * @ngdoc directive
 * @name desafioMuchmoreApp.directive:starButton
 * @description
 * # starButton
 */
angular.module('desafioMuchmoreApp')
  .directive('starButton', function () {
    return {
      scope: {
        store: "="
      },
      restrict: 'E',
      template: '<button class="btn btn-icon"><span class="glyphicon glyphicon-star" ng-class="{ active: store.star }"></span></button>',
      link: function(scope, elem) {
        elem.bind('click', function() {
          scope.$apply(function(){
            scope.store.star = !scope.store.star;
            console.log(scope.store);
          });
        });
      }
    };
  });
