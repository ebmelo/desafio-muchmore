'use strict';

/**
 * @ngdoc service
 * @name desafioMuchmoreApp.stores
 * @description
 * # stores
 * Factory in the desafioMuchmoreApp.
 */
angular.module('desafioMuchmoreApp')
  .factory('StoresService', function ($http, AppConfig) {

    function loadStores(){
      return $http.get(AppConfig.serviceUrl);
    }

    return {
      loadStores: loadStores
    };
  });
