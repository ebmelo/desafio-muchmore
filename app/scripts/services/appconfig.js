'use strict';

/**
 * @ngdoc service
 * @name desafioMuchmoreApp.appConfig
 * @description
 * # appConfig
 * Constant in the desafioMuchmoreApp.
 */
angular.module('desafioMuchmoreApp')
  .constant('AppConfig', {
    "serviceUrl": "https://sonaesodetapi-dev.herokuapp.com/api/v3/store?shoppingId=1"
  });
