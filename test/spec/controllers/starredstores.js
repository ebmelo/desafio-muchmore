'use strict';

describe('Controller: StarredstoresCtrl', function () {

  // load the controller's module
  beforeEach(module('desafioMuchmoreApp'));

  var StarredstoresCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StarredstoresCtrl = $controller('StarredstoresCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(StarredstoresCtrl.awesomeThings.length).toBe(3);
  });
});
