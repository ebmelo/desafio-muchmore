'use strict';

describe('Controller: StarredCtrl', function () {

  // load the controller's module
  beforeEach(module('desafioMuchmoreApp'));

  var StarredCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StarredCtrl = $controller('StarredCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(StarredCtrl.awesomeThings.length).toBe(3);
  });
});
