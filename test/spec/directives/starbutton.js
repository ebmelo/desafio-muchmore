'use strict';

describe('Directive: starButton', function () {

  // load the directive's module
  beforeEach(module('desafioMuchmoreApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<star-button></star-button>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the starButton directive');
  }));
});
